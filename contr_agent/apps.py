from django.apps import AppConfig


class ContrAgentConfig(AppConfig):
    name = 'contr_agent'
