from django.contrib import admin
from .models import *

admin.site.register(ContrAgent)
admin.site.register(ContrAgentCategory)
admin.site.register(ContrAgentCharacter)
admin.site.register(ContrAgentBalance)