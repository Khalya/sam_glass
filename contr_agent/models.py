from django.db import models
from django.contrib.auth.models import User
from contr_agent.choices import CONTR_AGENT_STATUS_CHOICES, CONTR_AGENT_BALANCE_CHOICES


class ContrAgentCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='Навание')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория агента'
        verbose_name_plural = 'Категории агентов'


class ContrAgent(models.Model):
    category = models.ForeignKey('ContrAgentCategory', on_delete=models.SET_NULL, verbose_name='Категория', null=True,
                                 blank=True)
    name = models.CharField(max_length=255, verbose_name='Имя', null=True, blank=True)
    org_name = models.CharField(max_length=255, verbose_name='Название организации')
    phone = models.CharField(max_length=255, verbose_name='Телефон', null=True, blank=True)
    phone2 = models.CharField(max_length=255, verbose_name='Телефон2', null=True, blank=True)
    address = models.CharField(max_length=255, verbose_name='Адресс', null=True, blank=True)
    inn = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(verbose_name='Адресс электронной почты', null=True, blank=True)
    extra_info = models.TextField(verbose_name='Информация', null=True, blank=True)
    status = models.CharField(max_length=255, choices=CONTR_AGENT_STATUS_CHOICES, verbose_name='Статус', null=True,
                              blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано', null=True, blank=True)
    update_at = models.DateTimeField(auto_now_add=True, verbose_name='Обновлено', null=True, blank=True)
    sms_live = models.CharField(max_length=255, verbose_name='sms', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Агент'
        verbose_name_plural = 'Агенты'


class ContrAgentBalance(models.Model):
    balance_type = models.CharField(max_length=255, choices=CONTR_AGENT_BALANCE_CHOICES, verbose_name='Тип', null=True,
                            blank=True)
    summa = models.FloatField(default=0, verbose_name='Сумма', null=True, blank=True)
    contr_agent = models.ForeignKey('ContrAgent', on_delete=models.CASCADE, verbose_name='Агент', null=True, blank=True)

    def __str__(self):
        return self.contr_agent.name

    class Meta:
        verbose_name = 'Баланс агента'
        verbose_name_plural = 'Балансы Агентов'


class ContrAgentCharacter(models.Model):
    contr_agent = models.ForeignKey('ContrAgent', on_delete=models.CASCADE, verbose_name='Агент', null=True, blank=True)
    name = models.CharField(max_length=255, verbose_name='Имя', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано', null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Характер агента'
        verbose_name_plural = 'Характеры агентов'
