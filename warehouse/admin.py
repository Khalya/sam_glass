from django.contrib import admin
from .models import *


class WarehouseAdmin(admin.ModelAdmin):
    pass


admin.site.register(Warehouse, WarehouseAdmin)
admin.site.register(WarehouseType)
admin.site.register(WarehouseIncome)
admin.site.register(WarehouseIncomeItem)
admin.site.register(WarehouseProduct)
