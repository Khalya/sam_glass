def calculate_self_price(first_price: object, first_count: object, second_price: object, second_count: object) -> object:
    """
    :param first_price:
    :param first_count:
    :param second_price:
    :param second_count:
    :return:
    """
    if first_price < 0:
        first_price = second_price
    if second_price < 0:
        second_price = first_price
    first_total = (float(first_price) * float(first_count))
    second_total = (float(second_price) * float(second_count))

    return (float(first_total) + float(second_total)) / (float(first_count) + float(second_count))