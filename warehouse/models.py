from django.db import models
from warehouse.choices import WAREHOUSE_STATUS_STATUS, WAREHOUSE_INCOME_STATUS


class WarehouseType(models.Model):
    name = models.CharField(max_length=255, verbose_name='Тип склада')

    def __str__(self):
        return self.name


class Warehouse(models.Model):
    warehouse_type = models.ForeignKey('WarehouseType', on_delete=models.PROTECT, verbose_name='Тип', null=True,
                                       blank=True)
    title = models.CharField(max_length=255, verbose_name='Название')
    status = models.CharField(max_length=255, choices=WAREHOUSE_STATUS_STATUS, verbose_name='Статус')

    def __str__(self):
        return self.title


class WarehouseIncome(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано', null=True, blank=True)
    total = models.FloatField(default=0, verbose_name='Сумма', null=True, blank=True)
    status = models.BigIntegerField(default=0, null=True, blank=True)
    contr_agent = models.ForeignKey('contr_agent.ContrAgent', on_delete=models.CASCADE, verbose_name='Агент', null=True,
                                    blank=True)
    warehouse = models.ForeignKey('Warehouse', on_delete=models.CASCADE, verbose_name='Склад', null=True, blank=True)

    def __str__(self):
        return self.contr_agent.name


class WarehouseProduct(models.Model):
    warehouse = models.ForeignKey('Warehouse', on_delete=models.CASCADE, verbose_name='Склад', null=True, blank=True)
    product = models.ForeignKey('product.Product', on_delete=models.SET_NULL, verbose_name='Продукты', null=True,
                                blank=True)
    self_price = models.FloatField(default=0, verbose_name='Цена', null=True, blank=True)
    count = models.FloatField(default=0, verbose_name='Количество', null=True, blank=True)

    def __str__(self):
        return self.warehouse.title


class WarehouseIncomeItem(models.Model):
    product = models.ForeignKey('product.Product', on_delete=models.SET_NULL, verbose_name='Продукты', null=True,
                                blank=True)
    count = models.FloatField(default=0, verbose_name='Количество', null=True, blank=True)
    price = models.FloatField(default=0, verbose_name='Цена', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано', null=True, blank=True)
    warehouse_income = models.ForeignKey('WarehouseIncome', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.product.name


class Order(models.Model):
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    contr_agent = models.ForeignKey('contr_agent.ContrAgent', verbose_name='Агент', null=True, blank=True,
                                    on_delete=models.SET_NULL)
    total = models.FloatField(default=0, verbose_name='сумма', null=True, blank=True)
    status = models.BigIntegerField(default=0, verbose_name='Статус', null=True, blank=True)

    def __str__(self):
        return self.contr_agent.name


class OrderItem(models.Model):
    product = models.ForeignKey('product.Product', verbose_name='продукт', on_delete=models.SET_NULL, null=True,
                                blank=True)
    count = models.FloatField(default=0, null=True, blank=True, verbose_name='Количество')
    price = models.FloatField(default=0, null=True, blank=True, verbose_name='Цена')
    order = models.ForeignKey('Order', on_delete=models.CASCADE, verbose_name='Заказ')

    def __str__(self):
        return self.product.name
