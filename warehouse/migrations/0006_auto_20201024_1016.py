# Generated by Django 3.1.2 on 2020-10-24 07:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_auto_20201023_2249'),
        ('warehouse', '0005_warehouseincome_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='warehouseincome',
            name='status',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='warehouseincomeitem',
            name='count',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='Количество'),
        ),
        migrations.AlterField(
            model_name='warehouseincomeitem',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Создано'),
        ),
        migrations.AlterField(
            model_name='warehouseincomeitem',
            name='price',
            field=models.FloatField(blank=True, default=0, null=True, verbose_name='Цена'),
        ),
        migrations.AlterField(
            model_name='warehouseincomeitem',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='product.productitem', verbose_name='Продукты'),
        ),
        migrations.AlterField(
            model_name='warehouseincomeitem',
            name='warehouse_income',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='warehouse.warehouseincome'),
        ),
    ]
