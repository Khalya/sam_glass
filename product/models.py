from django.db import models
from product.choices import PRODUCT_CATEGORY_SUB_CATEGORY, PRODUCT_ITEM_HOW_AREA, PRODUCT_ITEM_HOW_SIZE, \
    PRODUCT_CATEGORY_STATUS


class ProductCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя', null=True, blank=True)
    is_sub = models.CharField(max_length=255, verbose_name='Суб категория',
                              null=True, blank=True)
    main = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=255, choices=PRODUCT_CATEGORY_STATUS, verbose_name='Статус', null=True,
                              blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория продукта'
        verbose_name_plural = 'Категории продуктов'


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя', null=True, blank=True)
    cat = models.ForeignKey('ProductCategory', on_delete=models.SET_NULL, verbose_name='Категории', null=True,
                            blank=True)
    is_area = models.FloatField(default=0, verbose_name='Площадь', null=True, blank=True)
    how_area = models.CharField(max_length=255, verbose_name='Площадь', null=True, blank=True)
    is_size = models.CharField(verbose_name='Размер', max_length=255, null=True, blank=True)
    how_size = models.CharField(max_length=255, verbose_name='Размер', null=True, blank=True)
    status = models.CharField(max_length=255, choices=PRODUCT_CATEGORY_STATUS, verbose_name='Статус', null=True,
                              blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
