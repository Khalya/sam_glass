import datetime
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views import View
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView, TemplateView
from warehouse.models import Warehouse, WarehouseType, WarehouseIncome, WarehouseIncomeItem, WarehouseProduct, Order, \
    OrderItem
from product.models import Product, ProductCategory
from contr_agent.models import ContrAgentCategory, ContrAgent, ContrAgentBalance, ContrAgentCharacter
from warehouse.helpers import calculate_self_price


class WarehouseListView(ListView):
    template_name = 'warehouse/index.html'
    model = Warehouse
    success_url = '/'
    fields = '__all__'
    context_object_name = 'warehouse_list'


class WarehouseCreateView(CreateView):
    template_name = 'warehouse/WarehouseAdd.html'
    model = Warehouse
    success_url = '/'
    fields = '__all__'


class WarehouseUpdateView(UpdateView):
    template_name = 'warehouse/WarehouseUpdate.html'
    model = Warehouse
    success_url = '/'
    fields = '__all__'


class WarehouseTypeCreateView(CreateView):
    template_name = "warehouse/WarehouseTypeAdd.html"
    success_url = '/'
    fields = '__all__'
    model = WarehouseType


class WarehouseProductListView(ListView):
    template_name = "warehouse/WarehouseProduct.html"
    model = WarehouseProduct
    context_object_name = 'warehouse_product'


class IncomeListView(ListView):
    template_name = 'income/Income.html'
    model = WarehouseIncome
    context_object_name = 'income'


class IncomeCreateView(CreateView):
    template_name = 'income/IncomeAdd.html'
    model = WarehouseIncome
    success_url = '/income/'
    fields = ['total', 'contr_agent', 'warehouse']


class IncomeUpdateView(UpdateView):
    template_name = 'income/IncomeUpdate.html'
    model = WarehouseIncome
    success_url = '/income/'
    fields = ['total', 'contr_agent', 'warehouse']


class IncomeItemDetailView(TemplateView):
    template_name = 'income/IncomeDetail.html'

    def get_context_data(self, pk, **kwargs):
        context = super(IncomeItemDetailView, self).get_context_data(**kwargs)
        context['income'] = WarehouseIncome.objects.get(pk=pk)
        context['income_item'] = WarehouseIncomeItem.objects.filter(warehouse_income_id=pk)
        context['products'] = Product.objects.all()
        return context

    def post(self, request, pk, **kwargs):
        product = request.POST.get('product', '')
        count = request.POST.get('count', '')
        price = request.POST.get('price', '')
        WarehouseIncomeItem.objects.create(product_id=product, count=count,
                                           price=price, warehouse_income_id=pk)
        return redirect(reverse('income_item_detail', kwargs={'pk': pk}))


class WarehouseIncomeActions(View):

    def post(self, request, pk):
        action = request.POST.get('action', '')
        data = request.POST.get('data', '')
        if action == 'close_income':
            WarehouseIncome.objects.filter(pk=pk).update(status=1)
            income_items = WarehouseIncomeItem.objects.filter(warehouse_income_id=pk)
            for income_item in income_items:
                obj, created = WarehouseProduct.objects.get_or_create(product_id=income_item.product.pk,
                                                                      warehouse_id=data,
                                                                      defaults={'count': income_item.count,
                                                                                'self_price': income_item.price})
                if not created:
                    # Если обьект уже в базе
                    obj.self_price = calculate_self_price(obj.self_price, obj.count, income_item.price,
                                                          income_item.count)
                    obj.count += income_item.count
                    obj.save()
        elif action == 'delete_income_item':
            WarehouseIncomeItem.objects.filter(pk=data).delete()
        return redirect(reverse('income_item_detail', kwargs={'pk': pk}))


class ProductItemCreateView(CreateView):
    template_name = "product/ProductAdd.html"
    model = Product
    fields = '__all__'
    success_url = '/product/item/list/'


class ProductCategoryCreateView(CreateView):
    template_name = "product/ProductCategoryAdd.html"
    model = ProductCategory
    fields = '__all__'
    success_url = '/product/category/list/'


class ProductItemUpdateView(UpdateView):
    template_name = "product/ProductUpdate.html"
    model = Product
    fields = '__all__'
    success_url = '/product/item/list/'


class ProductCategoryUpdateView(UpdateView):
    template_name = "product/ProductCategoryUpdate.html"
    model = ProductCategory
    fields = '__all__'
    success_url = '/product/category/list/'


class ProductItemDeleteView(DeleteView):
    template_name = 'product/ProductDelete.html'
    model = Product
    success_url = '/product/item/list/'


class ProductCategoryDeleteView(DeleteView):
    template_name = 'product/ProductCategoryDelete.html'
    model = ProductCategory
    success_url = '/product/category/list/'


class ProductItemList(ListView):
    template_name = 'product/Product.html'
    model = Product
    context_object_name = 'products'


class ProductCategoryList(ListView):
    template_name = 'product/ProductCategory.html'
    model = ProductCategory
    context_object_name = 'category_products'


class AgentCreateView(CreateView):
    template_name = 'agent/create/AgentAdd.html'
    model = ContrAgent
    success_url = '/agent/'
    fields = '__all__'


class AgentBalanceCreateView(CreateView):
    template_name = 'agent/create/AgentBalanceAdd.html'
    model = ContrAgentBalance
    success_url = '/agent/balance/'
    fields = '__all__'


class AgentCategoryCreateView(CreateView):
    template_name = 'agent/create/AgentCategoryAdd.html'
    model = ContrAgentCategory
    success_url = '/agent/category/'
    fields = '__all__'


class AgentCharacterCreateView(CreateView):
    template_name = 'agent/create/AgentCharacterAdd.html'
    model = ContrAgentCharacter
    success_url = '/agent/character/'
    fields = '__all__'


class AgentUpdateView(UpdateView):
    template_name = 'agent/update/AgentUpdate.html'
    model = ContrAgent
    success_url = '/agent/'
    fields = '__all__'


class AgentCategoryUpdateView(UpdateView):
    template_name = 'agent/update/AgentCategoryUpdate.html'
    model = ContrAgentCategory
    success_url = '/agent/category/'
    fields = '__all__'


class AgentBalanceUpdateView(UpdateView):
    template_name = 'agent/update/AgentBalanceUpdate.html'
    model = ContrAgentBalance
    success_url = '/agent/balance/'
    fields = '__all__'


class AgentCharacterUpdateView(UpdateView):
    template_name = 'agent/update/AgentCharacterUpdate.html'
    model = ContrAgentCharacter
    success_url = '/agent/character/'
    fields = '__all__'


class AgentDeleteView(DeleteView):
    template_name = 'agent/delete/AgentDelete.html'
    model = ContrAgent
    success_url = '/agent/'
    fields = '__all__'


class AgentCategoryDeleteView(DeleteView):
    template_name = 'agent/delete/AgentCategoryDelete.html'
    model = ContrAgentCategory
    success_url = '/agent/category/'
    fields = '__all__'


class AgentCharacterDeleteView(DeleteView):
    template_name = 'agent/delete/AgentCharacterDelete.html'
    model = ContrAgentCharacter
    success_url = '/agent/character/'
    fields = '__all__'


class AgentBalanceDeleteView(DeleteView):
    template_name = 'agent/delete/AgentBalanceDelete.html'
    model = ContrAgentBalance
    success_url = '/agent/balance/'
    fields = '__all__'


class AgentListView(ListView):
    template_name = 'agent/list/Agent.html'
    model = ContrAgent
    success_url = '/agent/'
    context_object_name = 'agents'


class AgentCategoryListView(ListView):
    template_name = 'agent/list/AgentCategory.html'
    model = ContrAgentCategory
    context_object_name = 'agent_category'


class AgentBalanceListView(ListView):
    template_name = 'agent/list/AgentBalance.html'
    model = ContrAgentBalance
    context_object_name = 'agent_balance'


class AgentCharacterListView(ListView):
    template_name = 'agent/list/AgentCharacter.html'
    model = ContrAgentCharacter
    context_object_name = 'agent_character'


class OrderDetailView(TemplateView):
    template_name = 'warehouse/OrderDetail.html'

    def get_context_data(self, pk, **kwargs):
        context = super(OrderDetailView, self).get_context_data(**kwargs)
        context['order'] = Order.objects.get(id=pk)
        context['order_item'] = OrderItem.objects.filter(order_id=pk)
        context['products'] = Product.objects.all()
        return context

    def post(self, request, pk, **kwargs):
        product = request.POST.get('product', '')
        count = request.POST.get('count', '')
        price = request.POST.get('price', '')
        OrderItem.objects.create(product_id=product, count=count,
                                 price=price, order_id=pk)
        return redirect(reverse('order_detail', kwargs={'pk': pk}))


class OrderAction(View):
    def post(self, request, pk):
        action = request.POST.get('action', '')
        data = request.POST.get('data', '')
        if action == 'close_order':
            Order.objects.filter(pk=pk).update(status=1)
        elif action == 'delete_order_item':
            OrderItem.objects.filter(pk=data).delete()
        return redirect(reverse('order_detail', kwargs={'pk': pk}))


class OrderListView(ListView):
    template_name = 'warehouse/Order.html'
    model = Order
    context_object_name = 'order'


class OrderCreateView(CreateView):
    template_name = 'warehouse/OrderAdd.html'
    model = Order
    fields = ['contr_agent', 'total']
    success_url = '/order/'


class SearchView(TemplateView):
    template_name = 'filter/filter.html'

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        if self.request.method == 'POST':
            val = self.request.POST.get('filter', '')
            type = self.request.POST.get('type', '')
            if type == '1':
                context['filter'] = Warehouse.objects.filter(title__icontains=val)

            elif type == '3':
                context['agents'] = ContrAgent.objects.filter(name__icontains=val)
        return context

    def post(self, request, **kwargs):
        return render(request, self.template_name, self.get_context_data())
