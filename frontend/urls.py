from django.urls import path
from .views import WarehouseCreateView, WarehouseUpdateView, WarehouseListView, IncomeListView, WarehouseTypeCreateView, \
    IncomeCreateView, IncomeUpdateView, IncomeItemDetailView, WarehouseIncomeActions, ProductItemCreateView, \
    ProductCategoryCreateView, \
    ProductItemUpdateView, ProductCategoryUpdateView, ProductItemList, ProductCategoryList, ProductItemDeleteView, \
    ProductCategoryDeleteView, \
    AgentCreateView, AgentUpdateView, AgentCategoryCreateView, AgentCategoryUpdateView, AgentBalanceCreateView, \
    AgentBalanceUpdateView, \
    AgentCharacterCreateView, AgentCharacterUpdateView, AgentListView, AgentDeleteView, AgentCategoryListView, \
    AgentBalanceListView, \
    AgentCharacterListView, AgentCategoryDeleteView, AgentCharacterDeleteView, AgentBalanceDeleteView, OrderDetailView, \
    OrderAction, OrderListView, OrderCreateView, SearchView, WarehouseProductListView

urlpatterns = [
    # Склады
    path('', WarehouseListView.as_view(), name='home'),
    path('warehouse/create/', WarehouseCreateView.as_view(), name='create'),
    path('warehouse/type/create/', WarehouseTypeCreateView.as_view(), name='type_create'),
    path('warehouse/update/<int:pk>', WarehouseUpdateView.as_view(), name='update'),
    path('warehouse/product/', WarehouseProductListView.as_view(), name='warehouse_product'),
    # Приходы
    path('income/', IncomeListView.as_view(), name='income'),
    path('income/create/', IncomeCreateView.as_view(), name='income_create'),
    path('income/update/<int:pk>', IncomeUpdateView.as_view(), name='income_update'),
    path('income/item/detail/<int:pk>', IncomeItemDetailView.as_view(), name='income_item_detail'),
    path('income/item/action/<int:pk>', WarehouseIncomeActions.as_view(), name='income_actions'),
    # Продукты
    path('product/item/create/', ProductItemCreateView.as_view(), name='product_item_create'),
    path('product/category/create/', ProductCategoryCreateView.as_view(), name='product_category_item_create'),
    path('product/item/update/<int:pk>', ProductItemUpdateView.as_view(), name='product_item_update'),
    path('product/category/update/<int:pk>', ProductCategoryUpdateView.as_view(), name='product_category_item_update'),
    path('product/item/list/', ProductItemList.as_view(), name='product_list'),
    path('product/category/list/', ProductCategoryList.as_view(), name='product_category_list'),
    path('product/item/delete/<int:pk>', ProductItemDeleteView.as_view(), name='product_item_delete'),
    path('product/category/delete/<int:pk>', ProductCategoryDeleteView.as_view(), name='product_category_delete'),
    # Агенты
    path('agent/create/', AgentCreateView.as_view(), name='agent_create'),
    path('agent/update/<int:pk>', AgentUpdateView.as_view(), name='agent_update'),
    path('agent/delete/<int:pk>', AgentDeleteView.as_view(), name='agent_delete'),
    path('agent/category/create/', AgentCategoryCreateView.as_view(), name='agent_category_create'),
    path('agent/category/update/<int:pk>', AgentCategoryUpdateView.as_view(), name='agent_category_update'),
    path('agent/category/delete/<int:pk>', AgentCategoryDeleteView.as_view(), name='agent_category_delete'),
    path('agent/balance/create/', AgentBalanceCreateView.as_view(), name='agent_balance_create'),
    path('agent/balance/update/<int:pk>', AgentBalanceUpdateView.as_view(), name='agent_balance_update'),
    path('agent/balance/delete/<int:pk>', AgentBalanceDeleteView.as_view(), name='agent_balance_delete'),
    path('agent/character/create/', AgentCharacterCreateView.as_view(), name='agent_character_create'),
    path('agent/character/update/<int:pk>', AgentCharacterUpdateView.as_view(), name='agent_character_update'),
    path('agent/character/delete/<int:pk>', AgentCharacterDeleteView.as_view(), name='agent_character_delete'),
    path('agent/', AgentListView.as_view(), name='agent_list'),
    path('agent/category/', AgentCategoryListView.as_view(), name='agent_category_list'),
    path('agent/balance/', AgentBalanceListView.as_view(), name='agent_balance_list'),
    path('agent/character/', AgentCharacterListView.as_view(), name='agent_character_list'),
    #order
    path('order/detail/<int:pk>', OrderDetailView.as_view(), name='order_detail'),
    path('order/action/<int:pk>', OrderAction.as_view(), name='order_action'),
    path('order/', OrderListView.as_view(), name='order_list'),
    path('order/create/', OrderCreateView.as_view(), name='order_create'),
    path('search/', SearchView.as_view(), name='search'),
]
