from django import template

register = template.Library()


@register.filter(name='round')
def round_number(value):
    value = round(value)
    return str(value)
