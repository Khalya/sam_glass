from django.db import models

class OutCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

class OutLay(models.Model):
    category = models.ForeignKey('OutCategory', on_delete=models.PROTECT, verbose_name='Категории', null=True, blank=True)
    name = models.CharField(max_length=255, verbose_name='Имя')

    def __str__(self):
        return self.name