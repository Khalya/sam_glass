from django.db import models
from payment.choices import PAYEMNT_METHOD_VALYUTA, PAYMENT_TYPE_IS_DEFAULT, PAYMENT_LOG_SUMMA_TYPE, \
    PAYEMNT_LOG_SUMMA_METHOD


class PaymentMethods(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    valyuta = models.CharField(max_length=255, choices=PAYEMNT_METHOD_VALYUTA, verbose_name='Валюта')


class PaymentType(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название')
    is_default = models.CharField(max_length=255, choices=PAYMENT_TYPE_IS_DEFAULT)


class PaymentLog(models.Model):
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    summa = models.FloatField(default=0, verbose_name='Сумма')
    summa_type = models.CharField(max_length=255, choices=PAYMENT_LOG_SUMMA_TYPE)
    summa_method = models.CharField(max_length=255, choices=PAYEMNT_LOG_SUMMA_METHOD)
    outcat = models.IntegerField(default=0)
    outlay = models.IntegerField(default=0)
